'use strict';

angular.module('greatApp')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('postMaker', {
      'abstract': true,
      url: '',
      templateUrl: '/public/angular/views/root.html',
      controller: 'mainCntl',
      resolve: {
        postsResolution: ['Posts', function(Posts) {
          return Posts.init();
        }]
      }
    })
    .state('postMaker.home', {
      url: '/all',
      templateUrl: '/public/angular/views/post-list.html'
    })
    .state('postMaker.post', {
      url: '/post/:id',
      controller: 'singlePostCntl',
      templateUrl: '/public/angular/views/single-post.html'
    });

  $urlRouterProvider.otherwise('/all'); // takes a url not a state

}]);

