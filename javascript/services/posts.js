angular.module('greatApp').service('Posts', ['$http', function($http) {

  var service = {};

  service.posts = [];

  service.init = function() {
    return $http.get('https://gist.githubusercontent.com/clamstew/c7009faf56a58194efda/raw/066028643002b308eec9104447eeffcef6a4ff54/saved_posts.json')
      .then(function(response) {
        service.posts = response.data;
      });
  };

  service.find = function(id) {
    return _.find(service.posts, {'id': parseInt(id)});
  };

  return service;

}]);