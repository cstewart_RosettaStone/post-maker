angular.module('greatApp').controller('singlePostCntl',[
   '$stateParams',
   '$sce',
   '$scope',
   'Posts',
  function(
    $stateParams,
    $sce,
    $scope,
    Posts
  ) {

  $scope.post = Posts.find($stateParams.id);
  $scope.postUrl = $sce.trustAsResourceUrl($scope.post.url);

}]);