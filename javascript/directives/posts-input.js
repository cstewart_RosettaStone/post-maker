angular.module('greatApp').directive('postInput',[
  '$rootScope',
  'Posts',
  function(
    $rootScope,
    Posts
  ) {

  return {
    restrict: 'A',
    scope: {}, // isolates scope - we get our data instead from 'Posts' service
    controller: function($scope) {

      $scope.addPost = function() {
        var newPostObj = {title: $scope.newPost, url: $scope.newPostUrl};
        Posts.posts.push(newPostObj); // add new object to main posts array
        $scope.newPost = ''; // clear the title of post input
        $scope.newPostUrl = ''; // clear url input
        $rootScope.$broadcast('post:added'); // inform any listeners that a post has been added
      };

    },
    template: [
      "<input type='text' ng-model='newPost' placeholder='Add a post title' />",
      "<input type='text' ng-model='newPostUrl' placeholder='Add a url for post' />",
      "<button type='submit' ng-click='addPost()'>Add</button>"
    ].join('')
  }

}]);