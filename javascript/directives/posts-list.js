angular.module('greatApp').directive('postLists', [
  'Posts',
  function(
    Posts
  ) {

  return {
    restrict: 'A',
    scope: {},
    controller: function($scope) {
      // initialize scope with posts array from Posts service
      $scope.posts = Posts.posts;

      // refresh with what Posts.posts on a post added event
      $scope.$on('post:added', function() {
        $scope.posts = Posts.posts;
      });
    },
    template: [
      "<div ng-repeat='post in posts track by $index'>",
        "<a href='{{post.url}}' target='_blank'>{{post.title}}</a>",
        "&nbsp;&nbsp;<a ui-sref='postMaker.post({id: post.id})'>>></a>",
      "</div>"
    ].join('')

  }

}]);;